import React, {Component} from 'react';
import PropTypes from 'prop-types';


 class Toggler extends Component {
    state = {
      activeTogglerIndex: this.props.activeToggler
    };

    changeStatus = (selectedIndex)=>()=>{
      this.setState({
        activeTogglerIndex: selectedIndex
      })
    };
    render = () =>{
      let {children, name} = this.props;
      let {activeTogglerIndex} = this.state;

      return (
        <div>
        {name}
          <div className = "togglerContainer">
            {
              React.Children.map(
                children,
                (ChildrenItem, index) => {
                  if (index === activeTogglerIndex){
                    return React.cloneElement(ChildrenItem, {
                      name: ChildrenItem.props.name,
                      active: true,
                      changeStatus: this.changeStatus(index),
                    })
                  }else {
                    return React.cloneElement(ChildrenItem,{
                      name: ChildrenItem.props.name,
                      changeStatus: this.changeStatus(index),
                    })
                  }
                }
                    )
                  }

          </div>
        </div>
      );
    }

}




Toggler.propTypes = {
    name: PropTypes.string.isRequired,
    action: PropTypes.func.isRequired,
    activeState: PropTypes.string,
};

export default Toggler;
