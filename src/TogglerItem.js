import React from 'react';

const TogglerItem = ({name, active, changeStatus}) => {
    return (
        <div className={
            active === true
                ? "togglerItem active"
                : "togglerItem"
        }
             onClick={changeStatus}
        >
            {name}
        </div>
    );
};

export default TogglerItem;
