import React, { Component } from 'react';
import PropTypes from 'prop-types';


export class Form extends Component {
    state = {
      name: "Anonymus",
      password: "password",
      gender: "alien",
      age: "100",
      layout: "something",
      language: "nothing"

    };

    showData =(e) =>{
      e.preventDefault();
      console.log("name: ", this.state.name,
                  "password: ", this.state.password,
                  "gender: ", this.state.gender,
                  "age: ", this.state.age,
                  "layout: ", this.state.layout,
                  "language: ", this.state.language);
    }

    render = () =>{
      let {showData} = this;
      let {children} = this.props;


      return (
        <form>
         {children}
         <p><input type="submit" className = "togglerItem" onClick = {showData}></input></p>
         {
           React.Children.map(
             children,
             (ChildrenItem) => {
               for (let key in this.state){

               if (ChildrenItem.props.name === this.state[key]){
                 this.setState({
                   key: ChildrenItem.props.value
                 })
               }
             }

             }
                 )
               }
        </form>
      );
    }

}


export default Form;
