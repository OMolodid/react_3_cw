import React from 'react';
import './style/App.css';
import Toggler from "./Toggler";
import TogglerItem from "./TogglerItem";
import CustomInput from "./customInput";
import Form from "./form";


function App() {
    return (
        <div className="App">
        <Form>

        <CustomInput
        name = "name"
        type = "text"
        placeholder="Enter your name"
        >
        </CustomInput>

        <CustomInput
        name = "password"
        type = "password"
        placeholder="Enter your password"
        >
        </CustomInput>

            <Toggler
                name="gender"
                activeToggler={1}
                action={
                    () => {
                        console.log("action");
                    }
                }
            >
                <TogglerItem name="male">
                </TogglerItem>
                <TogglerItem name="female">
                </TogglerItem>
            </Toggler>

            <CustomInput
            name = "age"
            type = "number"
            placeholder="Enter your age"
            >
            </CustomInput>


            <Toggler
                name="layout"
                activeToggler={3}
                action={
                    () => {
                        console.log("action");
                    }
                }
            >
                <TogglerItem name="left">
                </TogglerItem>
                <TogglerItem name="center">
                </TogglerItem>
                <TogglerItem name="right">
                </TogglerItem>
                <TogglerItem name="baseline">
                </TogglerItem>
            </Toggler>

            <CustomInput
            name = "language"
            type = "text"
            placeholder="Enter your favorite Language"
            >
            </CustomInput>
            

            </Form>
        </div>
    );
}

export default App;
